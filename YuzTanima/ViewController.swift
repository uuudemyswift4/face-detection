//
//  ViewController.swift
//  YuzTanima
//
//  Created by Kerim Çağlar on 25/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Vision

class ViewController: UIViewController {

    @IBOutlet weak var myPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        faceDetection(image: self.myPhoto.image!)
    }
    
    func faceDetection(image:UIImage){
        self.myPhoto.image = image
        
        guard let ciImage = CIImage(image:image) else { return }
        
        let request = VNDetectFaceRectanglesRequest { (request, error) in
            
            if let error = error {
                print(error)
            } else {
                guard let faceObservation = request.results as? [VNFaceObservation] else { return }
                
                faceObservation.forEach({ (face) in
                    print(face.boundingBox)
                })
            }
        }
        
        let handler = VNImageRequestHandler(ciImage:ciImage)
        do{
            try handler.perform([request])
        }catch{
            print("HATA")
        }
    }
}

